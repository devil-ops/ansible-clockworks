# ansible-clockworks
> Ansible role for clockworks requests

This role uses the Clockworks REST API to build and edit hosts from your
inventory


## Installing / Getting started

First off, make sure you have an API key from clockworks at one of the links
below:

[Clockworks API Info](https://clockworks.oit.duke.edu/api_info)

[Clockworks Test API Info](https://clockworks-test.oit.duke.edu/api_info)

To use the role, do the following

```shell
git clone $this_repo
cd ansible-clockworks
vim your_hosts.inventory
ansible-playbook -i your.hosts ./build.yml
```

Running that code above will generate the hosts you specified in
your_hosts.inventory.  Multiple runs will ensure the hosts exist to the given
specs

You will be prompted for required variables upon running the playbook (netid,
key, fund code, etc).  If you wish to provide these non-interactively, use:


```shell
ansible-playbook -i your.hosts ./build.yml --extra-vars="user=netid password=apikey fund_code=123-1234 blep=bloop"
```

## Options

To see what options are available to you, for containers, networks and
locations, you can use the helper playbook 'options.yml'.  To use it, run:

```shell
ansible-playbook ./options.yml
```

This command will query the clockworks api and return a list of available options

## Decommisioning

Be careful here

To decommission/tear-down all hosts in your inventory, use:

```shell
ansible-playbook -i your.hosts ./teardown.yml
```

You will get dropped to an ansible pause task to give you one final break
before you destroy the hosts in your inventory, so use with caution

## Features

* Build hosts specified in your inventory
* Edit hosts in your inventory
* Decommision hosts in your inventory

## FAQ

### What is with all of the 'FAILED - RETRYING: Waiting for request to complete (1424 retries left)' messages?

These are normal.  This is ansible polling the API to see if the request has
completed.  It'll try this until the request is done, then move on

## Inventory Configuration

Use the group heading to specify what type of host you are building

```
[ubuntu16-selfadmin]
ubuntu16-selfadmin-ansible-test-01.oit.duke.edu cpu=3 ram=4
ubuntu16-selfadmin-ansible-test-02.oit.duke.edu cpu=2 ram=4

[ubuntu16-rapid]
ubuntu16-rapid-ansible-test-01.oit.duke.edu ram=4 hosting_level="silver"
ubuntu16-rapid-ansible-test-02.oit.duke.edu
ubuntu16-rapid-ansible-test-03.oit.duke.edu
```
